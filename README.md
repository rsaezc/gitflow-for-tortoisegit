# GitFlow for TortoiseGit
GitFlow UI addon for TortoiseGit.

## Dependencies
- Install python v3.7 or newer:
	- Ensure python is added to your PATH variable. 
- Install TortoiseGit v.2.0 or newer:
	- Ensure TortoiseGit is added to your PATH variable.
	- Select the credential settings, OpenSSH or TortoiseGitPlink.
- Install Git v.1.8 or newer:
	- Ensure Git is added to your PATH variable.
	- Ensure the credential settings are the same as TortoiseGit.
 
## Usage
This addon is designed to follow the GitFlow in an easy and comfortable way.

- Track the branch from which starting to work.
- If the branch does not exist, start a new one.
- Work in your repository and commit your changes as much as you want using TortoiseGit.
- Publish your changes frequently.
- When your work is finished, finish the branch. 

## Advices
The addon uses Git for automatic tasks and TortoiseGit to facilitate the most common tasks such as pulling, pushing and merging. However, it's not designed to allow an advance use of the repository.
While you are using the addon, you must follow these guidelines:

- You should never work in master and develop branches directly. If you need to do it, close the addon and use TortoiseGit directly.
- You should never create GitFlow branches manually. This can raise errors related to upstreams. If these errors happen, publish the branch and check "set upsteam" before pushing it.
- Don't close the addon after an error happens. Try to recover the error using this sequence: refresh branch list, track the branch again and publish your changes. This is mandatory if an error occurs while a branch is being finished.
- Ensure your repository doesn't ask for credentials. The addon is not designed to display a credential prompt. This can be accomplished using SSH. Note that both Git and TortoiseGit must be configured to use SSH credentials.
