import subprocess as cmd
import threading
import queue
from enum import Enum
import winreg
import os

_working_tree = ""
_remote_branches = []

class Event(Enum):
  ERROR = 0
  INFO = 1
  WARNING = 2
  DONE = 3

def runnable(function):
  def runnable_wrapper(*args, **kargs):
    event_queue = queue.Queue()
    args_tuple = args + (event_queue,)
    threading.Thread(target=function, args=args_tuple, kwargs=kargs).start()
    return event_queue
  return runnable_wrapper

def _run_command(command, shell=False):
  # Hide console window
  startup_info = cmd.STARTUPINFO()
  startup_info.dwFlags |= cmd.STARTF_USESHOWWINDOW
  if shell:
    startup_info.wShowWindow = 7 #SW_SHOWMINNOACTIVE: displays the window minimized and not activated. 

  try:
    output = cmd.run(command, capture_output=True, cwd=_working_tree,
                     check=True, text=True, startupinfo=startup_info)
    return output.stdout
  except cmd.CalledProcessError as error:
    source = "Git"
    if "TortoiseGitProc.exe" in error.cmd:
      source = "Tortoisegit"
    if error.stderr == "":
      raise GitFlowException(source + " command cancelled")
    else:
      raise GitFlowException(source + " error:\n" + error.stderr)
  except:
    raise GitFlowException("Could not run commands in directory " + _working_tree)

def _get_remote_branches():
  global _remote_branches
  _run_command("git fetch origin", shell=True)
  remote_branches = _run_command("git branch --list origin/* -r").replace(' ', '')
  _remote_branches = remote_branches.splitlines()

@runnable
def IsWorkingTreeCompatible(repository_path, event_queue):
  global _working_tree
  _working_tree = repository_path
  compatible = True
  try: 
    event_queue.put([Event.INFO, "Checking repository...\n"])
    event_queue.put([Event.INFO, "Checking remote..."])
    remotes = _run_command("git remote").replace(' ', '').splitlines()
    compatible = "origin" in remotes
    if not compatible:
      raise GitFlowException("Origin not found.")
    event_queue.put([Event.INFO, " Done.\nFetching remote branches..."])
    _get_remote_branches()
    event_queue.put([Event.INFO, " Done.\nChecking master branch..."])
    compatible = "origin/master" in _remote_branches
    if not compatible:
      raise GitFlowException("Master not found.")
    event_queue.put([Event.INFO, " Done.\nChecking develop branch..."])
    compatible = "origin/develop" in _remote_branches
    if not compatible:
      raise GitFlowException("Develop not found.")
    event_queue.put([Event.INFO, " Done.\n"])
    event_queue.put([Event.DONE, "Repository checked."])
  except GitFlowException as error:
    if not compatible:
      event_queue.put([Event.WARNING, " Failed.\n" + str(error) + " Repository is not compatible."])
      event_queue.put([Event.DONE, "Repository checked."])
    else:
      event_queue.put([Event.ERROR, " Failed.\n" + str(error)])

def _prepare_master_branch():
  _run_command("git checkout -f master")
  _run_command("git commit --allow-empty -m \"Starting GitFlow\"")
  _run_command("git push -f -u origin master", shell=True)

@runnable
def StartGitFlow(event_queue):
  event_queue.put([Event.INFO, "Starting GitFlow in " + _working_tree + "...\n"])
  event_queue.put([Event.INFO, "Fetching remote branches..."])
  try:
    _get_remote_branches()
    local_branches = _run_command("git branch").replace('*', ' ')
    local_branches = local_branches.replace(' ', '').splitlines()
    event_queue.put([Event.INFO, " Done.\nChecking master branch..."])
    if not "origin/master" in _remote_branches:
      if "master" in local_branches:
        _run_command("git branch -m master master_bak_from_gitflow")
      event_queue.put([Event.INFO, " Does not exist.\nCreating master branch..."])
      _prepare_master_branch()
    event_queue.put([Event.INFO, " Done.\nChecking develop branch..."])
    if not "origin/develop" in _remote_branches:
      event_queue.put([Event.INFO, " Does not exist.\Renaming master to develop..."])
      _run_command("git branch -f develop master")
      _run_command("git checkout -f develop")
      if "master" in local_branches:
        _run_command("git branch -D master")
      event_queue.put([Event.INFO, " Done.\nCreating a new master branch..."])
      _prepare_master_branch()
      event_queue.put([Event.INFO, " Done.\nPushing develop branch..."])
      _run_command("git push -u origin develop", shell=True)
    event_queue.put([Event.INFO, " Done.\n"])
    event_queue.put([Event.DONE, "GitFlow started."])
  except GitFlowException as error:
    event_queue.put([Event.ERROR, " Failed.\n" + str(error)])

@runnable
def GetBranches(event_queue):
  event_queue.put([Event.INFO, "Refreshing branch lists...\n"])
  event_queue.put([Event.INFO, "Fetching remote branches..."])
  try:
    _get_remote_branches()
    event_queue.put([Event.INFO, " Done.\n"])
    event_queue.put([Event.DONE, "Branch lists refreshed."])
  except GitFlowException as error:
    event_queue.put([Event.ERROR, " Failed.\n" + str(error)])

def GetBranchLists():
  features = [branch.replace("origin/feature/", "")
              for branch in _remote_branches
              if "feature/" in branch]
  releases = [branch.replace("origin/release/", "")
              for branch in _remote_branches
              if "release/" in branch]
  hotfixes = [branch.replace("origin/hotfix/", "")
              for branch in _remote_branches
              if "hotfix/" in branch]
  return [features, releases, hotfixes]

class GitFlowException(Exception):
    def __init__(self, message):
        self.message = message

class GitFlow(object):
    def __init__(self, prefix, base_branch, destination_branches):
        if type(self) == GitFlow:
            raise Exception("GitFlow must not be instantiated.")
        self._branchPrefix = prefix
        self._baseBranch = base_branch
        self._destBranches = destination_branches
        self._merge_failed = ""

    def __checkout_local_branch(self, branch):
      output = _run_command("git branch --list " + branch).strip()
      if output == "":
        _run_command("git checkout -f --track origin/" + branch)
      else:
        _run_command("git checkout -f " + branch)

    def __prepare_tortoisegit_pull_registry(self):
      self.registry = {}
      tortoise_key = "Software\\TortoiseGit"
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['OpenRebaseRemoteBranchEqualsHEAD'] = winreg.QueryValueEx(key, "OpenRebaseRemoteBranchEqualsHEAD")
        except:
          self.registry['OpenRebaseRemoteBranchEqualsHEAD'] = []
        winreg.SetValueEx(key, "OpenRebaseRemoteBranchEqualsHEAD", 0, winreg.REG_DWORD, 7)
        try:
          self.registry['OpenRebaseRemoteBranchUnchanged'] = winreg.QueryValueEx(key, "OpenRebaseRemoteBranchUnchanged")
        except:
          self.registry['OpenRebaseRemoteBranchUnchanged'] = []
        winreg.SetValueEx(key, "OpenRebaseRemoteBranchUnchanged", 0, winreg.REG_DWORD, 7)
        try:
          self.registry['OpenRebaseRemoteBranchFastForwards'] = winreg.QueryValueEx(key, "OpenRebaseRemoteBranchFastForwards")
        except:
          self.registry['OpenRebaseRemoteBranchFastForwards'] = []
        winreg.SetValueEx(key, "OpenRebaseRemoteBranchFastForwards", 0, winreg.REG_DWORD, 1)

      tortoise_key = tortoise_key + "\\TortoiseProc\\PullFetch\\" + _working_tree.replace(':', '_').replace("/", "\\") + "_1"
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['ffonly'] = winreg.QueryValueEx(key, "ffonly")
        except:
          self.registry['ffonly'] = [0, winreg.REG_DWORD]
        winreg.SetValueEx(key, "ffonly", 0, winreg.REG_DWORD, 1)
        try:
          self.registry['rebase'] = winreg.QueryValueEx(key, "rebase")
        except:
          self.registry['rebase'] = [0, winreg.REG_DWORD]
        winreg.SetValueEx(key, "rebase", 0, winreg.REG_DWORD, 1)

    def __restore_tortoisegit_pull_registry(self):
      tortoise_key = "Software\\TortoiseGit"
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        if self.registry['OpenRebaseRemoteBranchEqualsHEAD']:
          winreg.SetValueEx(key, "OpenRebaseRemoteBranchEqualsHEAD", 0,
                            self.registry['OpenRebaseRemoteBranchEqualsHEAD'][1],
                            self.registry['OpenRebaseRemoteBranchEqualsHEAD'][0])
        else:
          winreg.DeleteValue(key, "OpenRebaseRemoteBranchEqualsHEAD")

        if self.registry['OpenRebaseRemoteBranchUnchanged']:
          winreg.SetValueEx(key, "OpenRebaseRemoteBranchUnchanged", 0,
                            self.registry['OpenRebaseRemoteBranchUnchanged'][1],
                            self.registry['OpenRebaseRemoteBranchUnchanged'][0])
        else:
          winreg.DeleteValue(key, "OpenRebaseRemoteBranchUnchanged")

        if self.registry['OpenRebaseRemoteBranchFastForwards']:
          winreg.SetValueEx(key, "OpenRebaseRemoteBranchFastForwards", 0,
                            self.registry['OpenRebaseRemoteBranchFastForwards'][1],
                            self.registry['OpenRebaseRemoteBranchFastForwards'][0])
        else:
          winreg.DeleteValue(key, "OpenRebaseRemoteBranchFastForwards")

      tortoise_key = tortoise_key + "\\TortoiseProc\\PullFetch\\" + _working_tree.replace(':', '_').replace("/", "\\") + "_1"
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        winreg.SetValueEx(key, "ffonly", 0, self.registry['ffonly'][1], self.registry['ffonly'][0])
        winreg.SetValueEx(key, "rebase", 0, self.registry['rebase'][1], self.registry['rebase'][0])

    def __prepare_tortoisegit_push_registry(self):
      self.registry = {}
      working_tree_path = _working_tree.replace(':', '_').replace('/', '\\')
      tortoise_key = "Software\\TortoiseGit\\History\\PushRecurseSubmodules\\" + working_tree_path
      tortoise_key = tortoise_key.rpartition('\\')
      key_value = tortoise_key[2]
      tortoise_key = tortoise_key[0]
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['PushRecurseSubmodules'] = winreg.QueryValueEx(key, key_value)
        except:
          self.registry['PushRecurseSubmodules'] = [0, winreg.REG_DWORD]
        winreg.SetValueEx(key, key_value, 0, winreg.REG_DWORD, 0)

      tortoise_key = "Software\\TortoiseGit\\TortoiseProc\\Push\\" + working_tree_path
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['AllRemotes'] = winreg.QueryValueEx(key, "AllRemotes")
        except:
          self.registry['AllRemotes'] = [0, winreg.REG_DWORD]
        winreg.SetValueEx(key, "AllRemotes", 0, winreg.REG_DWORD, 0)
        try:
          self.registry['AllBranches'] = winreg.QueryValueEx(key, "AllBranches")
        except:
          self.registry['AllBranches'] = [0, winreg.REG_DWORD]
        winreg.SetValueEx(key, "AllBranches", 0, winreg.REG_DWORD, 0)

    def __restore_tortoisegit_push_registry(self):
      working_tree_path = _working_tree.replace(':', '_').replace('/', '\\')
      tortoise_key = "Software\\TortoiseGit\\History\\PushRecurseSubmodules\\" + working_tree_path
      tortoise_key = tortoise_key.rpartition('\\')
      key_value = tortoise_key[2]
      tortoise_key = tortoise_key[0]
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        winreg.SetValueEx(key, key_value, 0 , self.registry['PushRecurseSubmodules'][1],
                          self.registry['PushRecurseSubmodules'][0])
      
      tortoise_key = "Software\\TortoiseGit\\TortoiseProc\\Push\\" + working_tree_path
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        winreg.SetValueEx(key, "AllRemotes", 0, self.registry['AllRemotes'][1], self.registry['AllRemotes'][0])
        winreg.SetValueEx(key, "AllBranches", 0, self.registry['AllBranches'][1], self.registry['AllBranches'][0])

    def __prepare_tortoisegit_log_registry(self):
      self.registry = {}
      working_tree_parent = _working_tree.replace(':', '_').replace('/', '\\')
      working_tree_parent = working_tree_parent.rpartition('\\')
      key_value = working_tree_parent[2]
      working_tree_parent = working_tree_parent[0]

      tortoise_key = "Software\\TortoiseGit\\TortoiseProc\\ShowWholeProject\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['ShowWholeProject'] = winreg.QueryValueEx(key, key_value)
        except:
          self.registry['ShowWholeProject'] = [0, winreg.REG_DWORD]
        winreg.SetValueEx(key, key_value, 0, winreg.REG_DWORD, 0)

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\AllBranch\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['AllBranch'] = winreg.QueryValueEx(key, key_value)
        except:
          self.registry['AllBranch'] = [0, winreg.REG_DWORD]
        winreg.SetValueEx(key, key_value, 0, winreg.REG_DWORD, 0)

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\ShowTags\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['ShowTags'] = winreg.QueryValueEx(key, key_value)
        except:
          self.registry['ShowTags'] = [1, winreg.REG_DWORD]
        winreg.SetValueEx(key, key_value, 0, winreg.REG_DWORD, 1)

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\ShowLocalBranches\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['ShowLocalBranches'] = winreg.QueryValueEx(key, key_value)
        except:
          self.registry['ShowLocalBranches'] = [1, winreg.REG_DWORD]
        winreg.SetValueEx(key, key_value, 0, winreg.REG_DWORD, 1)

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\ShowRemoteBranches\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['ShowRemoteBranches'] = winreg.QueryValueEx(key, key_value)
        except:
          self.registry['ShowRemoteBranches'] = [1, winreg.REG_DWORD]
        winreg.SetValueEx(key, key_value, 0, winreg.REG_DWORD, 1)

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\ShowOtherRefs\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['ShowOtherRefs'] = winreg.QueryValueEx(key, key_value)
        except:
          self.registry['ShowOtherRefs'] = [1, winreg.REG_DWORD]
        winreg.SetValueEx(key, key_value, 0, winreg.REG_DWORD, 1)

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\ShowGravatar\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        try:
          self.registry['ShowGravatar'] = winreg.QueryValueEx(key, key_value)
        except:
          self.registry['ShowGravatar'] = [0, winreg.REG_DWORD]
        winreg.SetValueEx(key, key_value, 0, winreg.REG_DWORD, 0)

    def __restore_tortoisegit_log_registry(self):
      working_tree_parent = _working_tree.replace(':', '_').replace('/', '\\')
      working_tree_parent = working_tree_parent.rpartition('\\')
      key_value = working_tree_parent[2]
      working_tree_parent = working_tree_parent[0]

      tortoise_key = "Software\\TortoiseGit\\TortoiseProc\\ShowWholeProject\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        winreg.SetValueEx(key, key_value, 0, self.registry['ShowWholeProject'][1],
                          self.registry['ShowWholeProject'][0])  

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\AllBranch\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        winreg.SetValueEx(key, key_value, 0, self.registry['AllBranch'][1], self.registry['AllBranch'][0])

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\ShowTags\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        winreg.SetValueEx(key, key_value, 0, self.registry['ShowTags'][1], self.registry['ShowTags'][0])

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\ShowLocalBranches\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        winreg.SetValueEx(key, key_value, 0, self.registry['ShowLocalBranches'][1],
                          self.registry['ShowLocalBranches'][0])

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\ShowRemoteBranches\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        winreg.SetValueEx(key, key_value, 0, self.registry['ShowRemoteBranches'][1],
                          self.registry['ShowRemoteBranches'][0])

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\ShowOtherRefs\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        winreg.SetValueEx(key, key_value, 0, self.registry['ShowOtherRefs'][1],
                          self.registry['ShowOtherRefs'][0])

      tortoise_key = "Software\\TortoiseGit\\LogDialog\\ShowGravatar\\" + working_tree_parent
      with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, tortoise_key, 0, winreg.KEY_ALL_ACCESS) as key:
        winreg.SetValueEx(key, key_value, 0, self.registry['ShowGravatar'][1],
                          self.registry['ShowGravatar'][0])

    @runnable
    def Track(self, branch_name, event_queue=None):
      branch = self._branchPrefix + branch_name
      event_queue.put([Event.INFO, "Tracking the branch " + branch + "...\n"])
      event_queue.put([Event.INFO, "Checking out to the branch..."])
      try:
        self.__checkout_local_branch(branch)
        event_queue.put([Event.INFO, " Done.\nPreparing the TortoiseGit pull window..."])
        self.__prepare_tortoisegit_pull_registry()
        event_queue.put([Event.INFO, " Done.\nDisplaying the pull window..."])
        _run_command("TortoiseGitProc.exe /command:pull")
        event_queue.put([Event.INFO, " Done.\nRestoring the TortoiseGit pull window..."])
        self.__restore_tortoisegit_pull_registry()
        event_queue.put([Event.INFO, " Done.\n"])
        event_queue.put([Event.DONE, "Branch " + branch + " tracked."])
      except Exception as error:
        self.__restore_tortoisegit_pull_registry()
        event_queue.put([Event.ERROR, " Failed.\n" + str(error)])

    @runnable
    def Publish(self, branch_name, event_queue=None):
      branch = self._branchPrefix + branch_name
      event_queue.put([Event.INFO, "Publishing the branch " + branch + "...\n"])
      event_queue.put([Event.INFO, "Checking out to the branch..."])
      try:
        self.__checkout_local_branch(branch)
        event_queue.put([Event.INFO, " Done.\nPreparing the TortoiseGit push window..."])
        self.__prepare_tortoisegit_push_registry()
        event_queue.put([Event.INFO, " Done.\nDisplaying the push window..."])
        _run_command("TortoiseGitProc.exe /command:push")
        event_queue.put([Event.INFO, " Done.\nRestoring the TortoiseGit push window..."])
        self.__restore_tortoisegit_push_registry()
        event_queue.put([Event.INFO, " Done.\n"])
        event_queue.put([Event.DONE, "Branch " + branch + " published."])
      except Exception as error:
        self.__restore_tortoisegit_push_registry()
        event_queue.put([Event.ERROR, " Failed.\n" + str(error)])

    @runnable
    def Start(self, branch_name, event_queue):
      branch = self._branchPrefix + branch_name
      event_queue.put([Event.INFO, "Starting the new branch " + branch + "...\n"])
      event_queue.put([Event.INFO, "Preparing the TortoiseGit log window..."])
      try:
        self.__prepare_tortoisegit_log_registry()
        event_queue.put([Event.INFO, " Done.\nSelect the commit from which the new branch will be started..."])
        _run_command("TortoiseGitProc.exe /command:log /revend:" + self._baseBranch + " /outfile:tortoisegit_selection.tmp")
        try:
          selection_file = _working_tree + "\\tortoisegit_selection.tmp"
          with open(selection_file, 'r') as file:
            commit = file.readline()
          os.remove(selection_file)
          if not commit:
            raise FileNotFoundError()
        except FileNotFoundError:
          raise GitFlowException("No commit selected. Branch cannot be created")
        except Exception as error:
          raise GitFlowException(str(error))

        event_queue.put([Event.INFO, " Done.\nCreating the new branch..."])
        output = _run_command("git branch --list " + branch).strip()
        if output != "":
          _run_command("git branch -m " + branch + " " + branch + "_bak_from_gitflow")
        _run_command("git checkout -f -b " + branch + " " + commit)
        try:
          event_queue.put([Event.INFO, " Done.\nPushing the new branch..."])
          _run_command("git push -u origin " + branch, shell=True)
        except GitFlowException as error:
          _run_command("git checkout -f " + self._baseBranch)
          _run_command("git branch -d " + branch)
          raise GitFlowException("Perhaps " + branch + " exists on origin. Refresh the branch list\n\n" + str(error))
        event_queue.put([Event.INFO, " Done.\nRestoring the TortoiseGit log window..."])
        self.__restore_tortoisegit_log_registry()
        event_queue.put([Event.INFO, " Done.\nRefreshing branch lists..."])
        try:
          _get_remote_branches()
          event_queue.put([Event.INFO, " Done.\n"])
        except GitFlowException as error:
          event_queue.put([Event.WARNING, " Failed.\nRefresh the branch lists manually.\n"])
        event_queue.put([Event.DONE, "Branch " + branch + " started."])
      except Exception as error:
        self.__restore_tortoisegit_log_registry()
        event_queue.put([Event.ERROR, " Failed.\n" + str(error)])

    @runnable
    def Finish(self, branch_name, event_queue=None):
      branch = self._branchPrefix + branch_name
      event_queue.put([Event.INFO, "Finishing the branch " + branch + "...\n"])    
      try:
        event_queue.put([Event.INFO, "Fetching repository information..."])
        _run_command("git fetch origin", shell=True)
        event_queue.put([Event.INFO, " Done.\nChecking out to the branch..."])
        self.__checkout_local_branch(branch)
        event_queue.put([Event.INFO, " Done.\nChecking branch is up-to-date..."])
        head_local_branch = _run_command("git rev-parse HEAD")
        head_remote_branch = _run_command("git rev-parse @{u}")
        if head_local_branch != head_remote_branch:
          raise GitFlowException("Error: Branch " + branch + " is out to date")
        event_queue.put([Event.INFO, " Done.\n"])
        for destination_branch in self._destBranches:
          if not self._merge_failed or self._merge_failed == destination_branch:
            try:
              event_queue.put([Event.INFO, "Checking out to the destination branch " + destination_branch + "..."])
              _run_command("git checkout -f -B " + destination_branch + " --track origin/" + destination_branch)
              try:
                event_queue.put([Event.INFO, " Done.\nMerging branch " + branch + " into " + destination_branch + "..."])
                merge_output = _run_command("git merge --no-ff --no-commit " + branch)
                if "Already up to date" in merge_output: # Nothing to do
                  event_queue.put([Event.INFO, " Already up to date.\n"])
                  self._merge_failed = ""
                  continue
              except GitFlowException as error:
                if "command cancelled" in str(error):
                  try:
                    event_queue.put([Event.INFO, " Fail.\nResolve all conflicts before closing the TortoiseGit resolve window..."])
                    _run_command("TortoiseGitProc.exe /command:resolve")
                  except:
                    pass
                else:
                  raise
              event_queue.put([Event.INFO, " Done.\nCommitting the merge..."])
              _run_command("TortoiseGitProc.exe /command:commit")
              tags = ""
              if destination_branch == "master":
                tags = "--tags "
                event_queue.put([Event.INFO, " Done.\nAdd a tag to the merge..."])
                _run_command("TortoiseGitProc.exe /command:tag")
              event_queue.put([Event.INFO, " Done.\nPushing the merge to " + destination_branch + "..."])
              _run_command("git push -u " + tags + "origin " + destination_branch, shell=True)
              event_queue.put([Event.INFO, " Done.\n"])
              self._merge_failed = "" # Branch merged and pushed in destination
            except GitFlowException:
              self._merge_failed = destination_branch # Mark branch as failure
              raise
        self._merge_failed = "None" # Branch merged and pushed in all destinations but method can still fail
        event_queue.put([Event.INFO, "Branch " + branch + " merged into all destination branches.\nDeleting the branch..."])
        _run_command("git branch -d " + branch)
        _run_command("git push origin :" + branch, shell=True)
        self._merge_failed = "" # All OK, finished.
        event_queue.put([Event.INFO, " Done.\nRefreshing branch lists..."])
        try:
          _get_remote_branches()
          event_queue.put([Event.INFO, " Done.\n"])
        except GitFlowException as error:
          event_queue.put([Event.WARNING, " Failed.\nRefresh the branch lists manually.\n"])
        event_queue.put([Event.DONE, "Branch " + branch + " finished."])
      except GitFlowException as error:
        error_message = "Branch " + branch + " cannot be finished. It's mandatory to recover the branch.\n"
        error_message = error_message + "Ensure your network connection is working, try to publish your changes and track the branch.\n"
        error_message = error_message + "Then, finish the branch again.\n\n" + str(error)
        event_queue.put([Event.ERROR, " Failed.\n" + error_message])

class Features(GitFlow):
    def __init__(self):
      GitFlow.__init__(self, "feature/",
                       "develop", ["develop"])

class Releases(GitFlow):
    def __init__(self):
      GitFlow.__init__(self, "release/",
                       "develop", ["develop", "master"])

class Hotfixes(GitFlow):
    def __init__(self):
      GitFlow.__init__(self, "hotfix/",
                       "master", ["develop", "master"])
