from . import GitFlow
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import ttk
import queue
import os

class ReportDialog(tk.Toplevel):

  ErrorReported = False
  WarningReported = False
  __Finished = False

  def __init__(self, master=None, event_queue=None):
    tk.Toplevel.__init__(self, master)
    self.transient(master)
    self.geometry("+%d+%d" % (master.winfo_rootx()+50, master.winfo_rooty()+50))
    self.resizable(False, False)
    self.__InitializeWidgets()
    self.protocol("WM_DELETE_WINDOW", self.__DisableDeleteWindowEvent)
    self.grab_set()
    self.focus_set()
    self.__eventQueue = event_queue
    self.__ProcessEvents()
    self.master.wait_window(self)

  def __InitializeWidgets(self):
    self.dialogFrame = ttk.Frame(self)
    self.dialogFrame.grid(padx=5, pady=5)
    self.textScrollbar = tk.Scrollbar(self.dialogFrame)
    self.textScrollbar.grid(row=0, column=1, sticky=tk.N+tk.S)
    self.textInfo = tk.Text(self.dialogFrame, width=60, height=20, wrap=tk.WORD, yscrollcommand=self.textScrollbar.set)
    self.textScrollbar.config(command=self.textInfo.yview)
    self.textInfo.grid(row=0, column=0)
    self.textInfo.tag_config("error", foreground="red")
    self.textInfo.tag_config("done", foreground="blue")
    self.textInfo.tag_config("warning", foreground="orange")
    self.textInfo.config(state=tk.DISABLED)
    self.textMenu = tk.Menu(self.textInfo, tearoff=0)
    self.textMenu.add_command(label="Copy", command=self.__CopyText)
    self.textInfo.bind("<Button-3>", self.__PopupTextMenu)
    self.closeButton = ttk.Button(self.dialogFrame, command=self.__CloseDialog, text="Close")
    self.closeButton.grid(row=1, column=0, columnspan=2, padx=15, pady=5, sticky=tk.E)
    self.closeButton.state(["disabled"])

  def __PopupTextMenu(self, event):
    self.textMenu.post(event.x_root, event.y_root)

  def __CopyText(self):
    self.clipboard_clear()
    self.clipboard_append(self.textInfo.get(0.0, tk.END))

  def __CloseDialog(self):
    self.master.focus_set()
    self.destroy()

  def __DisableDeleteWindowEvent(self):
    if self.__Finished:
      self.__CloseDialog()
    else:
      pass

  def __ProcessEvents(self):
    queue_empty = True
    while queue_empty:
      try:
        event, data = self.__eventQueue.get_nowait()
        self.textInfo.config(state=tk.NORMAL)
        if event == GitFlow.Event.INFO:
          self.textInfo.insert(tk.END, data)
        elif event == GitFlow.Event.WARNING:
          self.textInfo.insert(tk.END, data, "warning")
          self.WarningReported = True
        elif event == GitFlow.Event.ERROR:
          self.textInfo.insert(tk.END, data, "error")
          self.closeButton.state(["!disabled"])
          self.ErrorReported = True
          self.__Finished = True
        elif event == GitFlow.Event.DONE:
          self.textInfo.insert(tk.END, data, "done")
          self.closeButton.state(["!disabled"])
          self.__Finished = True
        self.textInfo.config(state=tk.DISABLED)
        if self.__Finished:
          return
      except queue.Empty:
        break
    self.after(100, self.__ProcessEvents)

class GitFlowUI(ttk.Frame):
  def __init__(self, master=None):
    ttk.Frame.__init__(self, master)
    self.master.title("GitFlow addon for TortoiseGit")
    self.grid(padx=10, pady=10)
    self.__InitializeWidgets()
    self.master.resizable(False, False)
    self.master.update_idletasks()  # Update "requested size" from geometry manager
    x = (self.master.winfo_screenwidth() - self.master.winfo_reqwidth()) / 2
    y = (self.master.winfo_screenheight() - self.master.winfo_reqheight()) / 2
    self.master.geometry("+%d+%d" % (x, y))

  def __InitializeWidgets(self):
    self.repoPathLabel = ttk.Label(self, width=16, text="Repository path:")
    self.repoPathLabel.grid(row=0, column=0)
    self.repositoryPath = tk.StringVar()
    self.repoPathEntry = ttk.Entry(self, width=58, textvariable=self.repositoryPath)
    self.repoPathEntry.grid(row=0, column=1, columnspan=4)
    self.repoSelectionButton = ttk.Button(self, width=3, command=self.__GetRepositoryPathFromUser, text="...")
    self.repoSelectionButton.grid(row=0, column=5, padx=1, sticky=tk.E+tk.W)
    self.startButton = ttk.Button(self, command=self.__StartGitFlow, text="Start GitFlow")
    self.startButton.grid(row=0, column=6, sticky=tk.E+tk.W)

    self.featureLabelFrame = ttk.LabelFrame(self, text="Features")
    self.featureLabelFrame.grid(row=1, column=0, columnspan=2, padx=10, pady=10, ipadx=3)
    self.currentFeature = tk.StringVar()
    validateCommand = self.register(self.__featureComboboxOnUpdate)
    self.featureCombobox = ttk.Combobox(self.featureLabelFrame, width=17, textvariable=self.currentFeature,
                                        validate="key", validatecommand=(validateCommand, "%P"))
    self.featureCombobox.grid(row=0, column=0, columnspan=2, padx=6)
    self.featureCombobox.bind("<<ComboboxSelected>>", self.__featureComboboxOnSelect)
    self.__refreshIcon = tk.BitmapImage(file="modules/resources/refresh.xbm")
    self.refreshFeatureButton = ttk.Button(self.featureLabelFrame, command=self.__RefreshBranchLists, image=self.__refreshIcon)
    self.refreshFeatureButton.grid(row=0, column=2)
    self.startFeatureButton = ttk.Button(self.featureLabelFrame, command=self.__StartFeature, text="Start")
    self.startFeatureButton.grid(row=1, column=0, pady=3)
    self.finishFeatureButton = ttk.Button(self.featureLabelFrame, command=self.__FinishFeature, text="Finish")
    self.finishFeatureButton.grid(row=1, column=1, columnspan=2, pady=3)
    self.trackFeatureButton = ttk.Button(self.featureLabelFrame, command=self.__TrackFeature, text="Track")
    self.trackFeatureButton.grid(row=2, column=0, pady=3)
    self.publishFeatureButton = ttk.Button(self.featureLabelFrame, command=self.__PublishFeature, text="Publish")
    self.publishFeatureButton.grid(row=2, column=1, columnspan=2, pady=3)

    self.releaseLabelFrame = ttk.LabelFrame(self, text="Releases")
    self.releaseLabelFrame.grid(row=1, column=2, columnspan=2, pady=10, ipadx=3)
    self.currentRelease = tk.StringVar()
    validateCommand = self.register(self.__releaseComboboxOnUpdate)
    self.releaseCombobox = ttk.Combobox(self.releaseLabelFrame, width=17, textvariable=self.currentRelease,
                                        validate="key", validatecommand=(validateCommand, "%P"))
    self.releaseCombobox.grid(row=0, column=0, columnspan=2, padx=6)
    self.releaseCombobox.bind("<<ComboboxSelected>>", self.__releaseComboboxOnSelect)
    self.refreshReleaseButton = ttk.Button(self.releaseLabelFrame, command=self.__RefreshBranchLists, image=self.__refreshIcon)
    self.refreshReleaseButton.grid(row=0, column=2)
    self.startReleaseButton = ttk.Button(self.releaseLabelFrame, command=self.__StartRelease, text="Start")
    self.startReleaseButton.grid(row=1, column=0, pady=3)
    self.finishReleaseButton = ttk.Button(self.releaseLabelFrame, command=self.__FinishRelease, text="Finish")
    self.finishReleaseButton.grid(row=1, column=1, columnspan=2, pady=3)
    self.trackReleaseButton = ttk.Button(self.releaseLabelFrame, command=self.__TrackRelease, text="Track")
    self.trackReleaseButton.grid(row=2, column=0, pady=3)
    self.publishReleaseButton = ttk.Button(self.releaseLabelFrame, command=self.__PublishRelease, text="Publish")
    self.publishReleaseButton.grid(row=2, column=1, columnspan=2, pady=3)

    self.hotfixLabelFrame = ttk.LabelFrame(self, text="Hotfixes")
    self.hotfixLabelFrame.grid(row=1, column=4, columnspan=3, pady=10, ipadx=3)
    self.currentHotfix = tk.StringVar()
    validateCommand = self.register(self.__hotfixComboboxOnUpdate)
    self.hotfixCombobox = ttk.Combobox(self.hotfixLabelFrame, width=17, textvariable=self.currentHotfix,
                                        validate="key", validatecommand=(validateCommand, "%P"))
    self.hotfixCombobox.grid(row=0, column=0, columnspan=2, padx=6)
    self.hotfixCombobox.bind("<<ComboboxSelected>>", self.__hotfixComboboxOnSelect)
    self.refreshHotfixButton = ttk.Button(self.hotfixLabelFrame, command=self.__RefreshBranchLists, image=self.__refreshIcon)
    self.refreshHotfixButton.grid(row=0, column=2)
    self.startHotfixButton = ttk.Button(self.hotfixLabelFrame, command=self.__StartHotfix, text="Start")
    self.startHotfixButton.grid(row=1, column=0, pady=3)
    self.finishHotfixButton = ttk.Button(self.hotfixLabelFrame, command=self.__FinishHotfix, text="Finish")
    self.finishHotfixButton.grid(row=1, column=1, columnspan=2, pady=3)
    self.trackHotfixButton = ttk.Button(self.hotfixLabelFrame, command=self.__TrackHotfix, text="Track")
    self.trackHotfixButton.grid(row=2, column=0, pady=3)
    self.publishHotfixButton = ttk.Button(self.hotfixLabelFrame, command=self.__PublishHotfix, text="Publish")
    self.publishHotfixButton.grid(row=2, column=1, columnspan=2, pady=3)

    self.__DisableUI()

  def __EnableUI(self):
    self.featureCombobox.state(["!disabled"])
    self.startFeatureButton.state(["!disabled"])
    self.refreshFeatureButton.state(["!disabled"])
    self.releaseCombobox.state(["!disabled"])
    self.startReleaseButton.state(["!disabled"])
    self.refreshReleaseButton.state(["!disabled"])
    self.hotfixCombobox.state(["!disabled"])
    self.startHotfixButton.state(["!disabled"])
    self.refreshHotfixButton.state(["!disabled"])

  def __DisableUI(self):
    self.featureCombobox.state(["disabled"])
    self.startFeatureButton.state(["disabled"])
    self.finishFeatureButton.state(["disabled"])
    self.trackFeatureButton.state(["disabled"])
    self.publishFeatureButton.state(["disabled"])
    self.refreshFeatureButton.state(["disabled"])
    self.releaseCombobox.state(["disabled"])
    self.startReleaseButton.state(["disabled"])
    self.finishReleaseButton.state(["disabled"])
    self.trackReleaseButton.state(["disabled"])
    self.publishReleaseButton.state(["disabled"])
    self.refreshReleaseButton.state(["disabled"])
    self.hotfixCombobox.state(["disabled"])
    self.startHotfixButton.state(["disabled"])
    self.finishHotfixButton.state(["disabled"])
    self.trackHotfixButton.state(["disabled"])
    self.publishHotfixButton.state(["disabled"])
    self.refreshHotfixButton.state(["disabled"])

  def __featureComboboxOnSelect(self, event):
    self.startFeatureButton.state(["disabled"])
    self.finishFeatureButton.state(["!disabled"])
    self.trackFeatureButton.state(["!disabled"])
    self.publishFeatureButton.state(["disabled"])

  def __releaseComboboxOnSelect(self, event):
    self.startReleaseButton.state(["disabled"])
    self.finishReleaseButton.state(["!disabled"])
    self.trackReleaseButton.state(["!disabled"])
    self.publishReleaseButton.state(["disabled"])

  def __hotfixComboboxOnSelect(self, event):
    self.startHotfixButton.state(["disabled"])
    self.finishHotfixButton.state(["!disabled"])
    self.trackHotfixButton.state(["!disabled"])
    self.publishHotfixButton.state(["disabled"])

  def __featureComboboxOnUpdate(self, new_value):
    if new_value:
      if new_value in self.featureCombobox["values"]:
        self.__featureComboboxOnSelect(None)
        return True
    if self.startFeatureButton.instate(["disabled"]):
      self.startFeatureButton.state(["!disabled"])
      self.finishFeatureButton.state(["disabled"])
      self.trackFeatureButton.state(["disabled"])
      self.publishFeatureButton.state(["disabled"])
    return True

  def __releaseComboboxOnUpdate(self, new_value):
    if new_value:
      if new_value in self.releaseCombobox["values"]:
        self.__releaseComboboxOnSelect(None)
        return True
    if self.startReleaseButton.instate(["disabled"]):
      self.startReleaseButton.state(["!disabled"])
      self.finishReleaseButton.state(["disabled"])
      self.trackReleaseButton.state(["disabled"])
      self.publishReleaseButton.state(["disabled"])
    return True

  def __hotfixComboboxOnUpdate(self, new_value):
    if new_value:
      if new_value in self.hotfixCombobox["values"]:
        self.__hotfixComboboxOnSelect(None)
        return True
    if self.startHotfixButton.instate(["disabled"]):
      self.startHotfixButton.state(["!disabled"])
      self.finishHotfixButton.state(["disabled"])
      self.trackHotfixButton.state(["disabled"])
      self.publishHotfixButton.state(["disabled"])
    return True

  def __GetRepositoryPathFromUser(self):
    self.repositoryPath.set(filedialog.askdirectory())

  def __StartGitFlow(self):
    if not os.path.isdir(self.repositoryPath.get() + "\\.git"):
      messagebox.showerror("Error", "Path is not the root of the repository.", parent=self)
    else:
      event_queue = GitFlow.IsWorkingTreeCompatible(self.repositoryPath.get())
      report_dialog = ReportDialog(self, event_queue)
      if not report_dialog.ErrorReported and report_dialog.WarningReported:
        response = messagebox.askyesno("Repository not compatible","Do you want to start GitFlow in this repository?")
        if response == True:
          event_queue = GitFlow.StartGitFlow()
          report_dialog = ReportDialog(self, event_queue)
      if not report_dialog.ErrorReported and not report_dialog.WarningReported:
        self.__UpdateBranchLists()
        self.features = GitFlow.Features()
        self.releases = GitFlow.Releases()
        self.hotfixes = GitFlow.Hotfixes()
        self.__EnableUI()

  def __RefreshBranchLists(self):
    event_queue = GitFlow.GetBranches()
    report_dialog = ReportDialog(self, event_queue)
    if not report_dialog.ErrorReported:
      self.__UpdateBranchLists()

  def __TrackFeature(self):
      event_queue = self.features.Track(self.currentFeature.get())
      report_dialog = ReportDialog(self, event_queue)
      if not report_dialog.ErrorReported:
        self.publishFeatureButton.state(["!disabled"])
  def __PublishFeature(self):
      event_queue = self.features.Publish(self.currentFeature.get())
      report_dialog = ReportDialog(self, event_queue)
  def __StartFeature(self):
      current_branch = self.currentFeature.get()
      if not current_branch:
        messagebox.showerror("Error", "Select a branch or introduce a new one.", parent=self)
      else:
        event_queue = self.features.Start(current_branch)
        report_dialog = ReportDialog(self, event_queue)
        if not report_dialog.ErrorReported:
          self.publishFeatureButton.state(["!disabled"])
          self.__UpdateBranchLists()
        else:
          self.publishFeatureButton.state(["disabled"])
  def __FinishFeature(self):
      event_queue = self.features.Finish(self.currentFeature.get())
      report_dialog = ReportDialog(self, event_queue)
      if not report_dialog.ErrorReported:
        self.featureCombobox.state(["!disabled"])
        self.__UpdateBranchLists()
      else:
        self.featureCombobox.state(["disabled"])

  def __TrackRelease(self):
      event_queue = self.releases.Track(self.currentRelease.get())
      report_dialog = ReportDialog(self, event_queue)
      if not report_dialog.ErrorReported:
        self.publishReleaseButton.state(["!disabled"])
  def __PublishRelease(self):
      event_queue = self.releases.Publish(self.currentRelease.get())
      report_dialog = ReportDialog(self, event_queue)
  def __StartRelease(self):
      current_branch = self.currentRelease.get()
      if not current_branch:
        messagebox.showerror("Error", "Select a branch or introduce a new one.", parent=self)
      else:
        event_queue = self.releases.Start(current_branch)
        report_dialog = ReportDialog(self, event_queue)
        if not report_dialog.ErrorReported:
          self.publishReleaseButton.state(["!disabled"])
          self.__UpdateBranchLists()
        else:
          self.publishReleaseButton.state(["disabled"])
  def __FinishRelease(self):
      event_queue = self.releases.Finish(self.currentRelease.get())
      report_dialog = ReportDialog(self, event_queue)
      if not report_dialog.ErrorReported:
        self.releaseCombobox.state(["!disabled"])
        self.__UpdateBranchLists()
      else:
        self.releaseCombobox.state(["disabled"])

  def __TrackHotfix(self):
      event_queue = self.hotfixes.Track(self.currentHotfix.get())
      report_dialog = ReportDialog(self, event_queue)
      if not report_dialog.ErrorReported:
        self.publishHotfixButton.state(["!disabled"])
  def __PublishHotfix(self):
      event_queue = self.hotfixes.Publish(self.currentHotfix.get())
      report_dialog = ReportDialog(self, event_queue)
  def __StartHotfix(self):
      current_branch = self.currentHotfix.get()
      if not current_branch:
        messagebox.showerror("Error", "Select a branch or introduce a new one.", parent=self)
      else:
        event_queue = self.hotfixes.Start(current_branch)
        report_dialog = ReportDialog(self, event_queue)
        if not report_dialog.ErrorReported:
          self.publishHotfixButton.state(["!disabled"])
          self.__UpdateBranchLists()
        else:
          self.publishHotfixButton.state(["disabled"])
  def __FinishHotfix(self):
      event_queue = self.hotfixes.Finish(self.currentHotfix.get())
      report_dialog = ReportDialog(self, event_queue)
      if not report_dialog.ErrorReported:
        self.hotfixCombobox.state(["!disabled"])
        self.__UpdateBranchLists()
      else:
        self.hotfixCombobox.state(["disabled"])

  def __UpdateBranchLists(self):
    featuresList, releasesList, hotfixesList = GitFlow.GetBranchLists();
    self.featureCombobox["values"] = featuresList
    publish_button_enabled = self.publishFeatureButton.instate(["!disabled"])
    self.__featureComboboxOnUpdate(self.currentFeature.get())
    if self.startFeatureButton.instate(["!disabled"]): #current feature is not in the list.
        self.featureCombobox.set("")
    else:
        if publish_button_enabled: #restore publish button state
          self.publishFeatureButton.state(["!disabled"])

    self.releaseCombobox["values"] = releasesList
    publish_button_enabled = self.publishReleaseButton.instate(["!disabled"])
    self.__releaseComboboxOnUpdate(self.currentRelease.get())
    if self.startReleaseButton.instate(["!disabled"]): #current feature is not in the list.
        self.releaseCombobox.set("")
    else:
        if publish_button_enabled: #restore publish button state
          self.publishReleaseButton.state(["!disabled"])

    self.hotfixCombobox["values"] = hotfixesList
    publish_button_enabled = self.publishHotfixButton.instate(["!disabled"])
    self.__hotfixComboboxOnUpdate(self.currentHotfix.get())
    if self.startHotfixButton.instate(["!disabled"]): #current feature is not in the list.
        self.hotfixCombobox.set("")
    else:
        if publish_button_enabled: #restore publish button state
          self.publishHotfixButton.state(["!disabled"])
